#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
using namespace std;

struct Point { int x; int y; };
struct Human {
    int id;
    Point position;
};
struct RoundInput {
    Point hero_position;
    vector<Human> humans;

};

int readInt() {
    int i; cin >> i; return i;
}

RoundInput getRoundInput()
{
    RoundInput input;
    cin >> input.hero_position.x >> input.hero_position.y; cin.ignore();

    int humanCount = readInt();
    for (int i = 0; i < humanCount; ++i) {
        Human human{};
        cin >> human.id >> human.position.x >> human.position.y;
        cin.ignore();

        input.humans.push_back(human);
    }

    int zombieCount = readInt();
}

int main() {

    // game loop
    while (1) {

        for (int i = 0; i < humanCount; i++) {
            int humanId;
            int humanX;
            int humanY;
            cin >> humanId >> humanX >> humanY; cin.ignore();
        }
        int zombieCount;
        cin >> zombieCount; cin.ignore();
        for (int i = 0; i < zombieCount; i++) {
            int zombieId;
            int zombieX;
            int zombieY;
            int zombieXNext;
            int zombieYNext;
            cin >> zombieId >> zombieX >> zombieY >> zombieXNext >> zombieYNext; cin.ignore();
        }

        // Write an action using cout. DON'T FORGET THE "<< endl"
        // To debug: cerr << "Debug messages..." << endl;

        cout << "0 0" << endl; // Your destination coordinates
    }
}